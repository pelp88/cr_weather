package ru.codeinside.weather;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class Weather {
    public static void main(String[] args) {
        try {
            Document doc = Jsoup.connect("https://www.gismeteo.ru/weather-penza-4445/now/").get();
            System.out.println("Температура: " +
                    doc.select("[class=\"js_value tab-weather__value_l\"]").get(0).text());
            System.out.println("Ветер: " +
                    doc.select("[class=\"nowinfo__item nowinfo__item_wind\"]")
                            .select("[class=\"unit unit_wind_m_s\"]")
                            .select("[class=\"nowinfo__value\"]").text() + " м/с");
            System.out.println("Влажность сейчас: " +
                    doc.select("[class=\"nowinfo__item nowinfo__item_humidity\"]")
                            .select("[class=\"nowinfo__value\"]").text() + " %");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Ошибка: проверьте интернет-соединение!");
        }
    }
}

